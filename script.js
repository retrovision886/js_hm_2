const min = Math.floor(Math.random() * 60);
let b;

if (min < 15) {
    b = 'перша';
} else if (min < 30) {
    b = 'друга';
} else if (min < 45) {
    b = 'третя';
} else {
    b = 'четверта';
}

console.log(`Хвилини: ${min}\nЧверть: ${b}`);

// Задача дргуа

let lang = 'ua';
let arr;

if (lang === 'ua') {
    arr = ['Понеділок', 'Вівторок', 'Середа', 'Четвер', 'П\'ятниця', 'Субота', 'Неділя'];
} else if (lang === 'en') {
    arr = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
}

console.log(arr);



// з використанням багатовимірного масиву
let lang2 = 'ua';
const arr2 = {
    ua: ['Понеділок', 'Вівторок', 'Середа', 'Четвер', 'П\'ятниця', 'Субота', 'Неділя'],
    en: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
};

console.log(arr2[lang2]);

// Задача 3
let str = '112556'; // вхідний рядок з цифр
let sum1 = Number(str[0]) + Number(str[1]) + Number(str[2]); // обчислюємо суму 3-ох перших цифр
let sum2 = Number(str[str.length - 3]) + Number(str[str.length - 2]) + Number(str[str.length - 1]); // обчислюємо суму 3-ох останніх цифр

if (sum1 === sum2) {
    console.log('Щасливий!');
} else {
    console.log('Вам не пощастило');
}

